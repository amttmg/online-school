<?php

use App\SchoolClass;
use Illuminate\Database\Seeder;

class ClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $section = [
            ['name' => 'Sayapatri'],
            ['name' => 'Makhamali'],
            ['name' => 'Belee'],
            ['name' => 'Chameli'],
            ['name' => 'Parijat'],
            ['name' => 'Godawari'],
            ['name' => 'Sunakhari']
        ];
        $class = [
            [
                'name' => '1',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '2',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '3',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '4',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '5',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '5',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '7',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '8',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '9',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '10',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '11',
                'status' => 1,
                'sections' => $section
            ],
            [
                'name' => '12',
                'status' => 1,
                'sections' => [
                    ['name' => 'Physics'],
                    ['name' => 'Extra Maths'],
                    ['name' => 'Bio + Maths'],
                    ['name' => 'Management'],
                ]
            ],
        ];

        foreach ($class as $c) {
            $cls = SchoolClass::updateOrCreate(
                ['name' => $c['name']],
                [
                    'name' => $c['name'],
                    'status' => 1
                ]
            );
            foreach ($c['sections'] as $sec)
                $cls->sections()->updateOrCreate(
                    ['name' => $sec['name']],
                    [
                        'name' => $sec['name'],
                        'status' => 1
                    ]
                );
        }
    }
}
