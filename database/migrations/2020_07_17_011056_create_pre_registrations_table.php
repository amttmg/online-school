<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_registrations', function (Blueprint $table) {
            $table->id();
            $table->string('candidate_name');
            $table->string('gender');
            $table->string('father_name');
            $table->string('mother_name');
            $table->string('g_name');
            $table->string('phone');
            $table->string('g_phone');
            $table->string('school');
            $table->string('class');
            $table->string('stream');
            $table->string('t_province');
            $table->string('t_district');
            $table->string('t_metropolitan');
            $table->string('t_ward');
            $table->string('p_province');
            $table->string('p_district');
            $table->string('p_metropolitan');
            $table->string('p_ward');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_registrations');
    }
}
