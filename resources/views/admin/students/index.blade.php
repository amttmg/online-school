@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form class="form-inline" action="">
                    <div class="form-group ml-4">
                        <label for="class"><b>Class:</b></label>
                        <select name="class" id="select-class" class="form-control ml-4">
                            <option value="">All</option>
                            @foreach(getClasses() as $class)
                                <option value="{{ $class->id }}"> {{ $class->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group ml-4">
                        <label for="section"><b>Section:</b></label>
                        <select name="section" id="select-section" class="form-control ml-4">
                        </select>
                    </div>
                    <div class="form-group ml-4">
                        <label for="section"><b>Status:</b></label>
                        <div class="form-check ml-4">
                            <input
                                {{ request('is_verified')=='' ? 'checked' :'' }}
                                class="form-check-input" type="radio" name="is_verified" id="vfy1" value="">
                            <label class="form-check-label" for="vfy1">
                                All
                            </label>
                        </div>
                        <div class="form-check ml-4">
                            <input
                                {{ request('is_verified')=='1' ? 'checked' :'' }}
                                class="form-check-input" type="radio" name="is_verified" id="vfy2" value="1">
                            <label class="form-check-label" for="vfy2">
                                Approved
                            </label>
                        </div>
                        <div class="form-check ml-4">
                            <input
                                {{ request('is_verified')=='0' ? 'checked' :'' }}
                                class="form-check-input" type="radio" name="is_verified" id="vfy3" value="0">
                            <label class="form-check-label" for="vfy3">
                                Pending
                            </label>
                        </div>
                        {{-- <select name="is_verified" id="section" class="form-control">
                             <option {{ request('is_verified')=='' ? 'selected' :'' }} value="">All</option>
                             <option {{ request('is_verified')=='1' ? 'selected' :'' }} value="1">Approved
                             </option>
                             <option {{ request('is_verified')=='0' ? 'selected' :'' }} value="0">Not Approved
                             </option>
                         </select>--}}
                    </div>
                    <div class="form-group ml-4">
                        <label for="attendance"><b>Attendance:</b></label>
                        <div class="form-check ml-4">
                            <input
                                {{ request('attendance')=='' ? 'checked' :'' }}
                                class="form-check-input" type="radio" name="attendance" id="attendance1" value="">
                            <label class="form-check-label" for="attendance1">
                                All
                            </label>
                        </div>
                        <div class="form-check ml-4">
                            <input
                                {{ request('attendance')=='1' ? 'checked' :'' }}
                                class="form-check-input" type="radio" name="attendance" id="attendance2" value="1">
                            <label class="form-check-label" for="attendance2">
                                Present
                            </label>
                        </div>
                        <div class="form-check ml-4">
                            <input
                                {{ request('attendance')=='0' ? 'checked' :'' }}
                                class="form-check-input" type="radio" name="attendance" id="attendance3" value="0">
                            <label class="form-check-label" for="attendance3">
                                Absent
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-search"></i>
                            Search
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <hr/>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6"><h4>Students</h4></div>
                    <div class="col-md-6">
                        <a onclick="return confirm('Are you sure to reset all students device?')"
                           href="{{ route('reset.all') }}" class="btn btn-primary pull-right ">
                            <i class="fa fa-forward"></i>
                            Reset All devices
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table data-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Student Name</th>
                        <th>Phone No</th>
                        <th>Username</th>
                        <th>Class</th>
                        <th>Section</th>
                        <th>Attendance of today</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($students as $index=>$student)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $student->name }}</td>
                            <td>{{ $student->phone }}</td>
                            <td>{{ $student->username }}</td>
                            <td>{{ $student->schoolClass->name }}</td>
                            <td>{{ $student->schoolSection->name }}</td>
                            <td class="text-center">
                                @if(count($student->attendance))
                                    <span class="badge badge-primary">
                                <i class="fa fa-check"></i>
                                Preset
                            </span>
                                @else
                                    <span class="badge badge-danger">
                                <i class="fa fa-times"></i>
                                Absent
                            </span>
                                @endif
                            </td>
                            <td>
                                @if($student->is_verified)
                                    <span class="label label-primary">Approved</span>
                                @else
                                    <span class="label label-primary">Not Approved</span>
                                @endif
                            </td>
                            <td>
                                @if(!$student->is_verified)
                                    <a onclick="return confirm('Are you sure to approve?')"
                                       href="/admin/approve/{{ $student->id }}" class="btn btn-sm btn-primary">
                                        <i class="fa fa-check"></i>
                                        Approve
                                    </a>
                                    <a onclick="return confirm('Are you sure to reject?')"
                                       href="/admin/reject/{{ $student->id }}" class="btn btn-sm btn-danger">
                                        <i class="fa fa-trash"></i>
                                        Reject
                                    </a>
                                @else
                                    <a onclick="return confirm('Are you sure to reset device?')"
                                       href="/admin/reset-device/{{ $student->id }}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-gear"></i>
                                        Reset Device
                                    </a>
                                    <a onclick="return confirm('Are you sure to reset password?')"
                                       href="/admin/reset-password/{{ $student->id }}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-unlock"></i>
                                        Reset Password
                                    </a>
                                    <a onclick="return confirm('Are you sure to permanently delete?')"
                                       href="/admin/reject/{{ $student->id }}" class="btn btn-sm btn-danger">
                                        <i class="fa fa-trash"></i>
                                        Remove
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8" class="text-center">No Record Found</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
