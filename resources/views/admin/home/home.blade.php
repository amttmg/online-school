@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card mb-3">
            <div class="card-body">
                <h3 class="text-center">Welcome to BKVM online class admin panel.</h3>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <ul class="list-group" style="font-size: 20px">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Total registered students:
                        <span class="badge badge-primary badge-pill">{{ $data['totalStudentsCount'] }}</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Pending students:
                        <span class="badge badge-primary badge-pill">{{ $data['notApprovedStudentsCount'] }}</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Approved students:
                        <span class="badge badge-primary badge-pill">{{ $data['approvedStudentsCount'] }}</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Pre registered students for Class XI:
                        <span class="badge badge-primary badge-pill">{{ $data['preRegisteredStudentsCount'] }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
