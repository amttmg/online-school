<table>
    <tr>
        @foreach(array_keys($data[0]) as $head)
            <th>{{ $head }}</th>
        @endforeach
    </tr>

    @foreach($data as $row)
        <tr>
            @foreach($row as $key=>$value)
                <td>{{ $value }}</td>
            @endforeach
        </tr>
    @endforeach

</table>
