@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h4>Attendance Reports</h4></div>
            <div class="card-body">
                <form class="form-inline" method="POST" action="{{ route('reports.attendance') }}">
                    @csrf
                    <div class="form-group">
                        <label for="class">Class:&nbsp;&nbsp; </label>
                        <select name="class" id="select-class" class="form-control">
                            <option value="">All</option>
                            @foreach(getClasses() as $class)
                                <option value="{{ $class->id }}"> {{ $class->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="section">&nbsp;&nbsp;Section:&nbsp;&nbsp; </label>
                        <select name="section" id="select-section" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="from-date">&nbsp;&nbsp;From :&nbsp;&nbsp; </label>
                        <input type="date" max="{{ today()->toDateString() }}" value="{{request('from')}}" required name="from" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="from-date">&nbsp;&nbsp;To :&nbsp;&nbsp; </label>
                        <input type="date" max="{{ today()->toDateString() }}" value="{{request('to')}}" required name="to" class="form-control">
                    </div>
                    <div class="form-group">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button   type="submit" class="btn btn-primary">
                            <i class="fa fa-file-excel-o"></i>
                          Export to excel
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <hr/>
        <div class="card">
            <div class="card-header"><h4>Pre-Registration Reports</h4></div>

            <div class="card-body">&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="{{route('reports.pre-registration')}}" class="btn btn-primary">
                    <i class="fa fa-file-excel-o"></i>
                    Export to Excel
                </a>
            </div>
        </div>

    </div>
@endsection
