<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>BKVM Online School</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css" media="print">

    </style>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-dark bg-rbb-blue shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/login') }}">
                BKVM Online School
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                @guest

                @else
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item {{ (request()->is('admin/students')) ? 'active' : '' }}">
                            <a href="/admin/students" class="nav-link">Students</a>
                        </li>
                        {{-- <li class="nav-item {{ (request()->is('admin/teachers')) ? 'active' : '' }}">
                             <a href="/admin/teachers" class="nav-link">Teachers</a>
                         </li>--}}
                        <li class="nav-item {{ (request()->is('admin/o-class')) ? 'active' : '' }}">
                            <a href="/admin/o-class" class="nav-link">Online Class</a>
                        </li>
                        <li class="nav-item {{ (request()->is('admin/reports')) ? 'active' : '' }}">
                            <a href="/admin/reports" class="nav-link">Reports</a>
                        </li>
                    </ul>
            @endguest
            <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest

                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        <div class="container-fluid">
            @if(Session::has('message'))
                <div class="alert alert-{{session('message')['type']}}">
                    {{session('message')['text']}}
                </div>
            @endif
            <div class="row justify-content-center">
                @yield('content')
            </div>
        </div>
    </main>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#select-class').change(function () {
            $.ajax({
                url: "/get-section/" + $(this).val(),
                success: function (result) {
                    var $mySelect = $('#select-section');
                    $mySelect.html('');
                    $.each(result, function (key, value) {
                        var $option = $("<option/>", {
                            value: value.id,
                            text: value.name
                        });
                        $mySelect.append($option);
                    });
                }
            });
        });
        if ($("#select-class").length && $("#select-class").val() && $('#select-section').length) {
            $('#select-class').val({{ request('class') }}).trigger('change');
            $('#select-section').val({{ request('section') }}).trigger('change');
        }
        $('.data-table').DataTable(
            {
                "pageLength": 100
            }
        );
    })
</script>
</body>

</html>
