@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Teachers</div>
            <table class="table">
                <thead>
                <tr>
                    <th>Teachers Name</th>
                    <th>Class</th>
                    <th>Section</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($teachers as $teacher)
                    <tr>
                        <td>{{ $teacher->name }}</td>
                        <td>{{ $teacher->class }}</td>
                        <td>{{ $teacher->section }}</td>
                        <td>
                            @if($teacher->is_verified)
                                <span class="label label-primary">Approved</span>
                            @else
                                <a href="/admin/approve/{{ $teacher->id }}" class="btn btn-sm btn-primary">Approve</a>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">No Record Found</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="card-footer">
                {{$teachers->links()}}
            </div>
        </div>

    </div>
@endsection
