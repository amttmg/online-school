<div class="card">
    <div class="card-header">Add Class
    </div>
    <div class="card-body">
        <form action="{{ route('o-class.store') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="class">Class:</label>
                <select name="class" id="select-class" class="form-control @error('class') is-invalid @enderror">
                    <option value="">All</option>
                    @foreach(getClasses() as $class)
                        <option value="{{ $class->id }}"> {{ $class->name }}</option>
                    @endforeach
                </select>
                @error('class')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="section">&nbsp;&nbsp;Section:&nbsp;&nbsp; </label>
                <select name="section" id="select-section" class="form-control @error('section') is-invalid @enderror">
                </select>
                @error('section')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="meeting_id">Meeting ID</label>
                <input type="text" name="meeting_id" id="meeting_id"
                       class="form-control @error('meeting_id') is-invalid @enderror"/>
                @error('meeting_id')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="meeting_password">Meeting Password</label>
                <input type="text" name="meeting_password" id="meeting_password"
                       class="form-control @error('meeting_password') is-invalid @enderror"/>
                @error('meeting_password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <button class="btn btn-primary btn-block">
                <i class="fa fa-save"></i>
                Submit
            </button>
        </form>
    </div>
</div>
