@extends('admin.layouts.app')

@section('content')
    <online-classes></online-classes>
   {{-- <div class="col-md-4">
        @if(empty($meeting))
            @include('admin.classes._create')
        @else
            @include('admin.classes._edit', $meeting)
        @endif
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Active Meetings
            </div>
            <table class="table">
                <tr>
                    <th>Class</th>
                    <th>Section</th>
                    <th>Meeting Id</th>
                    <th>Meeting Password</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                @forelse($meetings as $meeting)
                    <tr>
                        <td>{{ $meeting->schoolClass->name }}</td>
                        <td>{{ $meeting->schoolSection->name }}</td>
                        <td>{{ $meeting->meeting_id }}</td>
                        <td>{{ $meeting->meeting_password }}</td>
                        <td>
                            @if($meeting->status)
                                <a href="{{ route('admin.o-class.active-toggle', $meeting->id) }}"
                                        class="btn btn-sm btn-primary">Running
                                </a>
                            @else
                                <a href="{{ route('admin.o-class.active-toggle', $meeting->id) }}"
                                        class="btn btn-sm btn-secondary">Disabled
                                </a>
                            @endif
                        </td>
                        <td>
                            <form class="form-inline" method="POST"
                                  action="{{ route('o-class.destroy', $meeting->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <div class="btn-group">
                                    <a href="{{ route('o-class.edit', $meeting->id) }}"
                                       class="btn btn-sm btn-primary">
                                        <i class="fa fa-edit"></i>
                                        Edit
                                    </a>
                                    <button onclick="return confirm('Are you sure to permanently delete?')"
                                            type="submit"
                                            class="btn btn-danger btn-sm"
                                    >
                                        <i class="fa fa-trash"></i>
                                        Delete
                                    </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">No record found</td>
                    </tr>
                @endforelse
            </table>
        </div>
    </div>--}}
@endsection
