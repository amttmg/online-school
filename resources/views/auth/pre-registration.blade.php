@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(Session::has('message'))
                    <div class="alert alert-{{session('message')['type']}}">
                        {{session('message')['text']}}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center">Pre registration form for class XI</h4>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('pre.registration.store') }}">
                            @csrf
                            <h4>Personal Information:</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="candidate_name"
                                               class="col-form-label ">{{ __('Candidate Name:') }}</label>

                                        <input id="candidate_name" type="text"
                                               class="form-control @error('candidate_name') is-invalid @enderror"
                                               name="candidate_name"
                                               value="{{ old('candidate_name') }}" autocomplete="candidate_name"
                                               autofocus>

                                        @error('candidate_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="father_name"
                                               class="col-form-label">{{ __('Father Name') }}</label>

                                        <input id="father_name" type="text"
                                               class="form-control @error('father_name') is-invalid @enderror"
                                               name="father_name"
                                               value="{{ old('father_name') }}" autocomplete="father_name" autofocus>

                                        @error('father_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="mother_name"
                                               class="col-form-label">{{ __('Mother Name') }}</label>

                                        <input id="mother_name" type="text"
                                               class="form-control @error('mother_name') is-invalid @enderror"
                                               name="mother_name"
                                               value="{{ old('mother_name') }}" autocomplete="father_name"
                                               autofocus>

                                        @error('mother_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="g_name"
                                               class="col-form-label">{{ __('Local Guardian Name') }}</label>

                                        <input id="g_name" type="text"
                                               class="form-control @error('g_name') is-invalid @enderror"
                                               name="g_name"
                                               value="{{ old('g_name') }}" autocomplete="g_name" autofocus>

                                        @error('g_name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="school"
                                               class="col-form-label">{{ __('School (Passed SEE from)') }}</label>

                                        <input id="school" type="text"
                                               class="form-control @error('school') is-invalid @enderror"
                                               name="school"
                                               value="{{ old('school') }}" autocomplete="school" autofocus>

                                        @error('school')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="class"
                                               class="col-form-label">{{ __('Class') }}</label>

                                        <select name="class" id="class"
                                                class="form-control @error('class') is-invalid @enderror">
                                            <option value="">Select Class</option>
                                            <option {{ old('class')=='11' ? 'selected' : '' }} value="11">XI
                                            </option>
                                        </select>
                                        @error('class')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="stream"
                                               class="col-form-label">{{ __('Stream') }}</label>

                                        <select name="stream" id="stream"
                                                class="form-control @error('stream') is-invalid @enderror">
                                            <option value="">Select Stream</option>
                                            <option
                                                {{ old('stream')=='Science' ? 'selected' : '' }} value="Science">
                                                Science
                                            </option>
                                            <option
                                                {{ old('stream')=='Management' ? 'selected' : '' }} value="Management">
                                                Management
                                            </option>
                                        </select>
                                        @error('stream')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="gender"
                                               class="col-form-label">{{ __('Gender') }}</label>
                                        <select name="gender" id="class"
                                                class="form-control @error('gender') is-invalid @enderror">
                                            <option value="">Select</option>
                                            <option {{ old('gender')=='M' ? 'selected' : '' }} value="M">Male
                                            <option {{ old('gender')=='F' ? 'selected' : '' }} value="F">Female
                                            <option {{ old('gender')=='O' ? 'selected' : '' }} value="O">Other
                                            </option>
                                        </select>

                                        @error('gender')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email"
                                               class="col-form-label">{{ __('Email Address') }}</label>

                                        <input id="email" type="text"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email"
                                               value="{{ old('email') }}" autocomplete="email" autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>

                            <h4>Phone</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone"
                                               class="col-form-label">{{ __('Phone Number') }}</label>

                                        <input id="phone" type="text"
                                               class="form-control @error('phone') is-invalid @enderror"
                                               name="phone"
                                               value="{{ old('phone') }}" autocomplete="phone" autofocus>

                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="g_phone"
                                               class="col-form-label">{{ __('Guardian Phone Number') }}</label>

                                        <input id="g_phone" type="text"
                                               class="form-control @error('g_phone') is-invalid @enderror"
                                               name="g_phone"
                                               value="{{ old('g_phone') }}" autocomplete="g_phone" autofocus>

                                        @error('g_phone')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <h4>Temporary Address:</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="t_province"
                                               class="col-form-label">{{ __('Province') }}</label>

                                        <input id="t_province" type="text"
                                               class="form-control @error('t_province') is-invalid @enderror"
                                               name="t_province"
                                               value="{{ old('t_province') }}" autocomplete="t_province" autofocus>

                                        @error('t_province')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="t_district"
                                               class="col-form-label ">{{ __('District') }}</label>

                                        <input id="t_district" type="text"
                                               class="form-control @error('t_district') is-invalid @enderror"
                                               name="t_district"
                                               value="{{ old('t_district') }}" autocomplete="t_district" autofocus>

                                        @error('t_district')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="t_metropolitan"
                                               class="col-form-label ">{{ __('Metropolitan/VDC') }}</label>

                                        <input id="t_metropolitan" type="text"
                                               class="form-control @error('t_metropolitan') is-invalid @enderror"
                                               name="t_metropolitan"
                                               value="{{ old('t_metropolitan') }}" autocomplete="t_metropolitan"
                                               autofocus>

                                        @error('t_metropolitan')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="t_ward"
                                               class="col-form-label ">{{ __('Ward No') }}</label>

                                        <input id="t_ward" type="text"
                                               class="form-control @error('t_ward') is-invalid @enderror"
                                               name="t_ward"
                                               value="{{ old('t_ward') }}" autocomplete="t_ward" autofocus>

                                        @error('t_ward')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <h4>Permanent Address:</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="same-address">
                                        <label class="form-check-label" for="exampleCheck1">Same as temporary
                                            address</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="p_province"
                                               class="col-form-label">{{ __('Province') }}</label>

                                        <input id="p_province" type="text"
                                               class="form-control @error('p_province') is-invalid @enderror"
                                               name="p_province"
                                               value="{{ old('p_province') }}" autocomplete="p_province" autofocus>

                                        @error('p_province')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="p_district"
                                               class="col-form-label ">{{ __('District') }}</label>

                                        <input id="p_district" type="text"
                                               class="form-control @error('p_district') is-invalid @enderror"
                                               name="p_district"
                                               value="{{ old('p_district') }}" autocomplete="p_district" autofocus>

                                        @error('p_district')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="p_metropolitan"
                                               class="col-form-label ">{{ __('Metropolitan/VDC') }}</label>

                                        <input id="p_metropolitan" type="text"
                                               class="form-control @error('p_metropolitan') is-invalid @enderror"
                                               name="p_metropolitan"
                                               value="{{ old('p_metropolitan') }}" autocomplete="p_metropolitan"
                                               autofocus>

                                        @error('p_metropolitan')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="p_ward"
                                               class="col-form-label ">{{ __('Ward No') }}</label>

                                        <input id="p_ward" type="text"
                                               class="form-control @error('p_ward') is-invalid @enderror"
                                               name="p_ward"
                                               value="{{ old('p_ward') }}" autocomplete="p_ward" autofocus>

                                        @error('p_ward')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block">
                                            <i class="fa fa-save"></i>
                                            {{ __('Submit') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $("#same-address").change(function () {
            if (this.checked) {
                $('#p_province').val($('#t_province').val())
                $('#p_district').val($('#t_district').val())
                $('#p_metropolitan').val($('#t_metropolitan').val())
                $('#p_ward').val($('#t_ward').val())
            }
        });
    </script>
@endsection
