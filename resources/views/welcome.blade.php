<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bal Kalyan Vidya Mandir (BKVM)</title>
    <!--

    Template 2103 Central

	http://www.tooplate.com/view/2103-central

    -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400">
    <!-- Google web font "Open Sans" -->
    <link rel="stylesheet" href="{{ asset('template/font-awesome-4.5.0/css/font-awesome.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/slick/slick.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('template/slick/slick-theme.css') }}"/>
    <link rel="stylesheet" href="{{ asset('template/css/tooplate-style.css') }}">
    <!-- tooplate style -->

    <script>
        var renderPage = true;

        if (navigator.userAgent.indexOf('MSIE') !== -1
            || navigator.appVersion.indexOf('Trident/') > 0) {
            /* Microsoft Internet Explorer detected in. */
            alert("Please view this in a modern browser such as Chrome or Microsoft Edge.");
            renderPage = false;
        }
    </script>

</head>

<body>
<!-- Loader -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<div class="container-fluid" style="margin: 0 !important;padding: 0px !important;">
    <section class="tm-section-head text-center" id="top">
        <header class="tm-text-gray">
            <div style="display: inline-block;">
                <img height="150px" src="{{ asset('template/img/slider/logo.jpg') }}" alt="Logo">
            </div>
            <div style="display: inline-block; vertical-align: middle;">
                <h1 style="font-family: fantasy">Bal Kalyan Vidya Mandir (BKVM)</h1>
                <h4> Biratnagar-11, Pichhra, Morang, Nepal</h4> 
                <h5> Phone No: 021-471560, 470295</h5>
                 <h5> E-mail: bkvmbrt@gmail.com</h5>
            </div>
        </header>
    </section>

    <section class="row" id="tm-section-1">
        <div class="col-lg-12 tm-slider-col">
            <div class="tm-img-slider">
                <div class="tm-img-slider-item" href="img/gallery-img-01.jpg">
                    {{-- <p class="tm-slider-caption">Amrit Tamang</p>--}}
                    <img src="{{ asset('template/img/slider/1.jpg') }}" alt="Image" class="tm-slider-img">
                </div>
                <div class="tm-img-slider-item" href="img/gallery-img-01.jpg">
                    {{--  <p class="tm-slider-caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                    <img src="{{ asset('template/img/slider/2.jpg') }}" alt="Image" class="tm-slider-img">
                </div>
                <div class="tm-img-slider-item" href="img/gallery-img-01.jpg">
                    {{--  <p class="tm-slider-caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                    <img src="{{ asset('template/img/slider/3.jpg') }}" alt="Image" class="tm-slider-img">
                </div>
                <div class="tm-img-slider-item" href="img/gallery-img-01.jpg">
                    {{-- <p class="tm-slider-caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                    <img src="{{ asset('template/img/slider/5.jpg') }}" alt="Image" class="tm-slider-img">
                </div>
                <div class="tm-img-slider-item" href="img/gallery-img-01.jpg">
                    {{-- <p class="tm-slider-caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                    <img src="{{ asset('template/img/slider/6.jpg') }}" alt="Image" class="tm-slider-img">
                </div>
                <div class="tm-img-slider-item" href="img/gallery-img-01.jpg">
                    {{--<p class="tm-slider-caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                    <img src="{{ asset('template/img/slider/7.jpg') }}" alt="Image" class="tm-slider-img">
                </div>
                <div class="tm-img-slider-item" href="img/gallery-img-01.jpg">
                    {{-- <p class="tm-slider-caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                    <img src="{{ asset('template/img/slider/8.jpg') }}" alt="Image" class="tm-slider-img">
                </div>
            </div>
        </div>
    </section>
</div>
<div class="container mt-3">
    <section class="tm-section-2 tm-section-mb">
        <div class="row">
            <div class="col-md-12 text-center m-2">
                <a href="/login" class="btn-primary btn">
                    <i class="fa fa-tv"></i>
                    Join Online Class
                </a>
                <a href="/pre-registration" class="btn-primary btn">
                    <i class="fa fa-edit"></i>
                    Pre Registration for class-XI
                </a>
            </div>
        </div>
    </section>
    {{-- <section class="tm-section-2 tm-section-mb" id="tm-section-2">
         <div class="row">
             <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-md-5 mb-5 pr-md-5">
                 <header class="text-center">
                     <i class="fa fa-4x fa-book pl-5 pb-5 pr-5 pt-2"></i>
                 </header>

                 <h2 class="text-center">About Us</h2>
                 <p class="text-center">
                     Bal Kalyan Vidya Mandir School, established in 2040 BS, is located in the heart of the city
                     Biratnagar-ll, Janpath Tole and has admitted about 3000 students. The school also known as B.K.V.M
                     is a centre of quality education and has been able to gain and maintain a great reputation since the
                     year of establishment.
                 </p>
             </div>
             <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-md-5 mb-5 pr-md-5">
                 <header class="text-center">
                     <i class="fa fa-4x fa-trophy pl-5 pb-5 pr-5 pt-2"></i>
                 </header>

                 <h2 class="text-center">Awards</h2>
                 <p class="text-center">
                     It has been adding glory to its history by often being awarded with regional and national prize. In
                     the SLC Board of 2060, 2065, 2067, 2070, 2072, 2073 B.S. It was awarded with the title of ' Eastern
                     Region Topper', in the year of 2065 with
                     'S.LC. Board First', Mr. Rakesh Giri.
                     The history was repeated when Mr. Arun Kalikote stood S.L.C. Board First in the year 2068 B.S. And
                     again, in the year 2073 B.S. it has been ranked first among all the private schools of Nepal.
                 </p>
             </div>
             <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-md-5 mb-5 pr-md-5">
                 <header class="text-center">
                     <i class="fa fa-4x fa-building pl-5 pb-5 pr-5 pt-2"></i>
                 </header>

                 <h2 class="text-center">Introduction</h2>
                 <p class="text-center">
                     2882 (Two thousand Eight Hundred Eighty Two) students have been studying from pre-primary
                     level to the secondary level including 994 (Nine Hundred Ninety Four) females and 1,888 (One
                     Thousand Eight Hundred Eighty Eight) males. The school operates its academic of 76 sections in over
                     94 classrooms, 6 labs (science and computer), 2 library rooms, 6 department rooms and 5
                     administrative offce rooms. Focusing the demand of yearly extending school's catchment area, 12
                     school buses provide the facility of transportation.
                 </p>
             </div>
         </div>
     </section>

     <section class="tm-section-3 tm-section-mb" id="tm-section-3">
         <div class="row">
             <div class="col-md-6 tm-mb-sm-4 tm-2col-l">
                 <div class="image">
                     <img src="{{ asset('template/img/tm-img-1.jpg') }}" class="img-fluid"/>
                 </div>
                 <div class="tm-box-3">
                     <h2>Lorem ipsum dolor</h2>
                     <p> Cras aliquet urna ut sapien tincidunt, quis malesuada elit facilisis. Vestibulum sit amet tortor
                         velit. Nam elementum nibh a libero pharetra elementum. Maecenas feugiat ex purus, quis volutpat
                         lacus placerat malesuada.</p>
                     --}}{{--<div class="text-center">
                         <a href="#tm-section-5" class="btn btn-big">Details</a>
                     </div>--}}{{--
                 </div>
             </div>
             <div class="col-md-6 tm-2col-r">
                 <div class="image">
                     <img src="{{ asset('template/img/tm-img-2.jpg')}}" class="img-fluid"/>
                 </div>
                 <div class="tm-box-3">
                     <header>
                         <h2>Vestibulum sit amet</h2>
                     </header>
                     <p> Cras aliquet urna ut sapien tincidunt, quis malesuada elit facilisis. Vestibulum sit amet tortor
                         velit. Nam elementum nibh a libero pharetra elementum. Maecenas feugiat ex purus, quis volutpat
                         lacus placerat malesuada.</p>
                    --}}{{-- <div class="text-center">
                         <a href="#tm-section-5" class="btn btn-big">Details</a>
                     </div>--}}{{--
                 </div>
             </div>
         </div>
     </section>

     <section class="tm-section-4 tm-section-mb" id="tm-section-4">
         <div class="row">

         </div>
     </section>

     <section class="tm-section-5" id="tm-section-5">
         <div class="row">
             <div class="col-lg-4 col-md-12 col-sm-12">
                 <div class="image fit">
                     <img src="{{ asset('template/img/tm-sc4-img-1.jpg')}}" class="img-fluid">
                 </div>
             </div>

             <div class="col-lg-8 col-md-12 col-sm-12 pl-lg-0">
                 <div class="media tm-media">
                     <img src="{{ asset('template/img/sc4-img-2.jpg')}}" class="img-responsive tm-media-img">
                     <div class="media-body tm-box-5">
                         <h2>Etiam tincidunt ullamcorper</h2>
                         <p class="mb-0">Maecenas tempor nibh sed rhoncus ullamcorper. Ut porttitor ante non accumsan
                             pretium. Maecenas iaculis arcu sed porta accumsan.</p>
                     </div>
                 </div>
                 <div class="media tm-media">
                     <img src="{{ asset('template/img/sc4-img-3.jpg') }}" class="img-responsive tm-media-img">
                     <div class="media-body tm-box-5">
                         <h2>Sed et finibus tortor</h2>
                         <p class="mb-0">Suspendisse iaculis leo libero, ut congue augue feugiat eu. Nulla faucibus non
                             odio sed auctor. Vestibulum a tincidunt dolor, eget eleifend lacus.</p>
                     </div>
                 </div>
                 <div class="media tm-media">
                     <img src="{{ asset('template/img/sc4-img-4.jpg') }}" class="img-responsive tm-media-img">
                     <div class="media-body tm-box-5">
                         <h2>Vestibulum sit amet</h2>
                         <p class="mb-0"> Cras aliquet urna ut sapien tincidunt, quis malesuada elit facilisis.
                             Vestibulum sit amet tortor velit. Nam elementum nibh a libero pharetra elementum. </p>
                     </div>
                 </div>
             </div>
         </div>
     </section>

     <section class="tm-section-6" id="tm-section-6">
         <div class="row">
             <div class="col-lg-7 col-md-7 col-xs-12">
                 <div class="contact_message">
                     <form action="" method="post" class="contact-form">
                         @csrf
                         <div class="row mb-2">
                             <div class="form-group col-xl-6">
                                 <input type="text" id="contact_name" name="contact_name" class="form-control"
                                        placeholder="Name" required/>
                             </div>
                             <div class="form-group col-xl-6 pl-xl-1">
                                 <input type="email" id="contact_email" name="contact_email" class="form-control"
                                        placeholder="Email" required/>
                             </div>
                         </div>
                         <div class="form-group">
                             <textarea id="contact_message" name="contact_message" class="form-control" rows="6"
                                       placeholder="Message" required></textarea>
                         </div>
                         <button type="submit" class="btn  tm-btn-submit float-right btn-big">Send It Now</button>
                     </form>
                 </div>
             </div>

             <div class="col-lg-5 col-md-5 col-xs-12 tm-contact-right">
                 <div class="tm-address-box">
                     <h2 class="mb-4">Contact Us</h2>
                     <p class="mb-5">Bal Kalyan Vidhya Mandir(BKVM)</p>
                     <address>
                         Biratnagar-8, Pichhra, Morang, Nepal
                         <br> Phone: 021-571560/470296,
                         <br> Email: mail@bkvm.edu.np
                     </address>
                 </div>
             </div>
         </div>
     </section>--}}
    <footer class="mt-5">
        <p class="text-center">Copyright © {{ today()->year }} Bal Kalyan Vidhya Mandir(BKVM)</p>
    </footer>
</div>

<!-- load JS files -->
<script type="text/javascript" src="{{ asset('template/js/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('template/js/popper.min.js') }}"></script>
<!-- https://popper.js.org/ -->
<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>
<!-- https://getbootstrap.com/ -->
<script type="text/javascript" src="{{ asset('template/slick/slick.min.js') }}"></script>
<!-- Slick Carousel -->

<script>
    function setCarousel() {
        var slider = $('.tm-img-slider');

        if (slider.hasClass('slick-initialized')) {
            slider.slick('destroy');
        }

        if ($(window).width() > 991) {
            // Slick carousel
            slider.slick({
                autoplay: true,
                fade: true,
                speed: 300,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        } else {
            slider.slick({
                autoplay: true,
                fade: true,
                speed: 300,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        }
    }

    $(document).ready(function () {
        if (renderPage) {
            $('body').addClass('loaded');
        }

        setCarousel();

        $(window).resize(function () {
            setCarousel();
        });

        // Close menu after link click
        $('.nav-link').click(function () {
            $('#mainNav').removeClass('show');
        });

        // https://css-tricks.com/snippets/jquery/smooth-scrolling/
        // Select all links with hashes
        $('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function (event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top + 1
                        }, 1000, function () {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            }
                            ;
                        });
                    }
                }
            });
    });
</script>

</body>

</html>
