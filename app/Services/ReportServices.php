<?php


namespace App\Services;


use App\AcType;
use App\Loan;
use App\LoanClass;
use App\Period;

class ReportServices
{
    public function getReportData($periodId)
    {
        $acTypes     = AcType::all();
        $loanClasses = LoanClass::active()->get();
        $master      = [];
        foreach ($acTypes as $acType) {
            $row            = [];
            $total          = 0;
            $totalProvision = 0;
            foreach ($loanClasses as $class) {
                $sum                        = Loan::where('loan_class_id', $class->id)
                    ->where('ac_type_id', $acType->id)
                    ->where('period_id', $periodId)
                    ->sum('limit');
                $provision                  = ($sum * $class->provision) / 100;
                $row[$class->name]          = $sum;
                $row[$class->provision.'%'] = $provision;
                $total                      += $sum;
                $totalProvision             += $provision;
            }
            $row['Total']           = $total;
            $row['Total Provision'] = $totalProvision;
            $master[$acType->code]  = $row;
        }


        $master['Total']    = $this->getTotal($periodId, $loanClasses);
        $master['Previous'] = $this->getPreviousData($periodId, $loanClasses);
        $diff               = [];
        foreach ($master['Total'] as $key => $cell) {
            $diff[$key] = $cell - $master['Previous'][$key];
        }
        $master['Difference'] = $diff;

        return $master;
    }

    private function getTotal($periodId, $loanClasses)
    {
        //For Total last row
        $sumRow         = [];
        $total          = 0;
        $totalProvision = 0;
        foreach ($loanClasses as $class) {
            $sum                           = Loan::where('period_id', $periodId)->where('loan_class_id',
                $class->id)->sum('limit');
            $provision                     = ($sum * $class->provision) / 100;
            $sumRow[$class->name]          = $sum;
            $sumRow[$class->provision.'%'] = $provision;

            $total          += $sum;
            $totalProvision += $provision;
        }
        $sumRow['Total']           = $total;
        $sumRow['Total Provision'] = $totalProvision;

        return $sumRow;
    }

    public function getNpa($periodId)
    {
        $performing    = Loan::where('period_id', $periodId)
            ->whereIn('loan_class_id', [1, 2])->sum('limit');
        $nonPerforming = Loan::where('period_id', $periodId)
            ->whereIn('loan_class_id', [3, 4, 5])->sum('limit');
        $npa           = ($nonPerforming / ($performing + $nonPerforming)) * 100;

        return ['performing' => $performing, 'non-performing' => $nonPerforming, 'npa' => round($npa, 2)];
    }

    private function getPreviousData($periodId, $loanClasses)
    {
        $period = Period::find($periodId);
        $query  = Period::where('user_id', auth()->id());
        if ($period->quarter == 1) {
            $query = $query->where('year', ($period->year - 1))->where('quarter', 4);
        } else {
            $query = $query->where('year', $period->year)->where('quarter', ($period->quarter - 1));
        }
        $previousPeriod = $query->first();
        $id             = 0;
        if ($previousPeriod) {
            $id = $previousPeriod->id;
        }

        return $this->getTotal($id, $loanClasses);
    }
}
