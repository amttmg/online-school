<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    protected $fillable = ['name', 'status'];

    public function sections()
    {
        return $this->hasMany(SchoolSection::class);
    }
}
