<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_logged_in',
        'is_verified',
        'class',
        'section',
        'phone',
        'role',
        'username',
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeStudents($query)
    {
        $query->where('role', 'student');
    }

    public function scopeApprovedStudents($query)
    {
        $query->where('role', 'student')->where('is_verified', 1);
    }

    public function scopeNotApprovedStudents($query)
    {
        $query->where('role', 'student')->where('is_verified', 0);
    }

    public function scopeTeachers($query)
    {
        $query->where('role', 'teacher');
    }

    public function attendance()
    {
        return $this->hasMany(Attendance::class, 'student_id', 'id');
    }

    public function SchoolClass()
    {
        return $this->belongsTo(SchoolClass::class, 'class', 'id');
    }

    public function SchoolSection()
    {
        return $this->belongsTo(SchoolSection::class, 'section', 'id');
    }

}
