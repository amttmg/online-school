<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable = [
        'meeting_id',
        'meeting_password',
        'status',
        'class',
        'section',
    ];

    public function scopeActive($query)
    {
        $query->where('status', 1);
    }

    public function SchoolClass()
    {
        return $this->belongsTo(SchoolClass::class, 'class', 'id');
    }

    public function SchoolSection()
    {
        return $this->belongsTo(SchoolSection::class, 'section', 'id');
    }

}
