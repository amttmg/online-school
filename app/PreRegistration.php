<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreRegistration extends Model
{
    protected $fillable = [
        'candidate_name',
        'gender',
        'father_name',
        'mother_name',
        'g_name',
        'phone',
        'g_phone',
        'school',
        'class',
        'stream',
        't_province',
        't_district',
        't_metropolitan',
        't_ward',
        'p_province',
        'p_district',
        'p_metropolitan',
        'p_ward',
        'email',
    ];

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
