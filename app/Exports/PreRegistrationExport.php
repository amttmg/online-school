<?php

namespace App\Exports;

use App\PreRegistration;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PreRegistrationExport implements FromCollection, WithHeadings
{

    public function collection()
    {
        return PreRegistration::all();
    }

    public function headings(): array
    {
        return (new \App\PreRegistration)->getTableColumns();
    }
}
