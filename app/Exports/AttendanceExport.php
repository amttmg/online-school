<?php

namespace App\Exports;

use App\Attendance;
use App\User;
use Carbon\CarbonPeriod;
use http\Env\Response;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class AttendanceExport implements FromView
{
    private $data;

    /**
     * AttendanceExport constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    private function getData()
    {
        $class = $this->data["class"] ?? null;
        $section = $this->data["section"] ?? null;
        $from = $this->data["from"];
        $to = $this->data["to"];
        $query = User::approvedStudents()->select('users.id', 'users.name', 'users.phone', 'school_classes.name as class', 'school_sections.name as section')
            ->join('school_classes', 'school_classes.id', '=', 'users.class')
            ->join('school_sections', 'school_sections.id', '=', 'users.section');
        if ($class) {
            $query->where('class', $class);
        }
        if ($section) {
            $query->where('section', $section);
        }

        if ($from && $to) {
            $period = CarbonPeriod::create($from, $to);
            $students = $query->get();
            $students->map(function ($student) use ($period) {
                foreach ($period as $date) {
                    $date = $date->toDateString();
                    $student->{$date} = $this->getAttendance($student->id, $date);
                }
            });

        } else {
            $students = [];
        }
        return $students;
    }

    private function getAttendance($studentId, $date)
    {
        return Attendance::where('student_id', $studentId)
            ->whereDate('attendance', $date)
            ->exists() ? 'P' : 'A';
    }

    public function view(): View
    {
        $data = $this->getData()->toArray();
        return view('admin.reports._attendance', compact('data'));
    }
}
