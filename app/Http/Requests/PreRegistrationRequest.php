<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PreRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'candidate_name' => 'required',
            'gender' => 'required',
            'father_name' => 'required',
            'mother_name' => 'required',
            'g_name' => 'required',
            'phone' => 'required|max:10|min:10',
            'g_phone' => 'required|max:10|min:10',
            'school' => 'required',
            'class' => 'required',
            'stream' => 'required',
            't_province' => 'required',
            't_district' => 'required',
            't_metropolitan' => 'required',
            't_ward' => 'required',
            'p_province' => 'required',
            'p_district' => 'required',
            'p_metropolitan' => 'required',
            'p_ward' => 'required',
            'email' => 'required|email',
        ];
    }
}
