<?php

namespace App\Http\Controllers;

use App\Meeting;
use App\PreRegistration;
use App\User;

class HomeController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $meeting = Meeting::where('class', $user->class)
            ->where('section', $user->section)
            ->where('status', 1)
            ->first();

        $data['totalStudentsCount'] = User::students()->count();
        $data['approvedStudentsCount'] = User::approvedStudents()->count();
        $data['notApprovedStudentsCount'] = User::notApprovedStudents()->count();
        $data['preRegisteredStudentsCount'] = PreRegistration::count();

        return view($user->role . '.home.home', compact('meeting', 'data'));
    }
}
