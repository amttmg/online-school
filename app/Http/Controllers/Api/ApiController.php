<?php

namespace App\Http\Controllers\Api;

use App\SchoolClass;
use App\SchoolSection;

class ApiController extends \App\Http\Controllers\ApiController
{
    public function getClasses()
    {
        $classes = SchoolClass::all();

        return response($classes);
    }

    public function getSections($id)
    {
        $classes = SchoolSection::where('school_class_id', $id)->get();

        return response($classes);
    }
}
