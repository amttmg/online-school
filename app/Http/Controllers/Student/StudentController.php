<?php


namespace App\Http\Controllers\Student;


use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;

class StudentController extends Controller
{
    public function setAttendance($id)
    {
        $student = User::findOrFail($id);
        $student->attendance()->create([
            'attendance' => Carbon::now(),
        ]);

        return response(['success' => true]);
    }
}
