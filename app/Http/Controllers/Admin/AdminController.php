<?php


namespace App\Http\Controllers\Admin;


use App\Exports\AttendanceExport;
use App\Exports\PreRegistrationExport;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    public function students(Request $request)
    {
        $query = User::students()->with(['schoolClass', 'schoolSection']);
        foreach ($request->only('class', 'section') as $key => $value) {
            if ($value) {
                $query->where($key, $value);
            }
        }
        if ($request->is_verified == '0') {
            $query->where('is_verified', '0');
        } elseif ($request->is_verified == '1') {
            $query->where('is_verified', '1');
        }
        if ($request->attendance == '0') {
            $query->whereDoesntHave('attendance', function ($query) {
                $query->whereDate('attendance', '=', Carbon::today()->toDateString());
            });
        } elseif ($request->attendance == '1') {
            $query->whereHas('attendance', function ($query) {
                $query->whereDate('attendance', '=', Carbon::today()->toDateString());
            });
        }

        $query->with([
            'attendance' => function ($query) {
                $query->whereDate('attendance', '=', Carbon::today()->toDateString());
            },
        ]);

        $students = $query->get();

        return view('admin.students.index', compact('students'));
    }

    public function teachers()
    {
        $teachers = User::teachers()->paginate(10);

        return view('admin.teachers.index', compact('teachers'));
    }

    public function approve($id)
    {
        $user              = User::find($id);
        $user->is_verified = !$user->is_verified;
        $user->save();

        return back()->with('message',
            ['text' => 'Successfully Approved.', 'type' => 'success']);
    }

    public function reject($id)
    {
        $user = User::find($id);
        $user->delete();

        return back()->with('message',
            ['text' => 'Successfully Rejected.', 'type' => 'success']);
    }

    public function resetDevice($id)
    {
        $user               = User::find($id);
        $user->is_logged_in = 0;
        $user->save();

        return back()->with('message',
            ['text' => 'Successfully reset.Student can login now.', 'type' => 'success']);
    }

    public function resetDeviceAll()
    {

        $affected = DB::table('users')
            ->where('is_logged_in', true)
            ->update(['is_logged_in' => false]);

        return back()->with('message',
            [
                'text' => 'Successfully reset. Students can login now. Affected student Count : '.$affected,
                'type' => 'success',
            ]);
    }

    public function resetPassword($id)
    {
        $user               = User::find($id);
        $user->password     = Hash::make('student123');
        $user->is_logged_in = 0;
        $user->save();

        return back()->with('message',
            [
                'text' => 'Successfully reset password. Now username: '.$user->email.' and password: student123.',
                'type' => 'success',
            ]);
    }

    public function reports()
    {
        return view('admin.reports.report');

    }

    public function preRegistrationReports()
    {
        return Excel::download(new PreRegistrationExport(), 'pre-registration.xlsx');
    }

    public function attendanceReport(Request $request)
    {
        return Excel::download(new AttendanceExport($request->only('class', 'section', 'from', 'to')),
            'attendance-report.xlsx');
    }
}
