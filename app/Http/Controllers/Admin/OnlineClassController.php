<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\OnlineClassRequest;
use App\Meeting;
use App\User;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class OnlineClassController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return response(Meeting::with(['SchoolClass', 'SchoolSection'])->get());
        }

        return view('admin.classes.index');
    }

    public function store(OnlineClassRequest $request)
    {
        Meeting::create([
            'class'            => $request->class,
            'section'          => $request->section,
            'meeting_id'       => $request->meeting_id,
            'meeting_password' => $request->meeting_password,
        ]);

        return back()->with('message',
            ['text' => 'Successfully Created.', 'type' => 'success']);
    }

    public function edit($id)
    {
        $meetings = Meeting::with(['schoolClass', 'schoolSection'])->get();
        $meeting  = Meeting::findOrFail($id);

        return view('admin.classes.index', compact('meetings', 'meeting'));
    }

    public function update(OnlineClassRequest $request, $id)
    {
        $meeting = Meeting::findOrFail($id);

        $res = $meeting->update([
            'class'            => $request->class,
            'section'          => $request->section,
            'meeting_id'       => $request->meeting_id,
            'meeting_password' => $request->meeting_password,
        ]);

        return response($res);
    }

    public function destroy($id)
    {
        $meeting = Meeting::find($id);
        $res     = $meeting->delete();

        return response($res);
    }

    public function activeToggle($id)
    {
        $meeting         = Meeting::find($id);
        $meeting->status = !$meeting->status;

        return $meeting->save();
    }
}
