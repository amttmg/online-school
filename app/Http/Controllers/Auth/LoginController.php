<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    private $userName;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->userName = $this->findUsername();
    }

    public function username()
    {
        return $this->userName;
    }

    public function authenticated(Request $request, $user)
    {
        $user->is_logged_in = true;
        $user->save();
    }

    public function logout(Request $request)
    {
        $user = auth()->user();
        $user->is_logged_in = false;
        $user->save();
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect('/login');
    }

    protected function credentials(Request $request)
    {
        $user = User::where($this->username(), $request->login)->first();
        if ($user && $user->role == "student") {
            return array_merge($request->only($this->username(), 'password'), ['is_logged_in' => 0]);
        }

        return array_merge($request->only($this->username(), 'password'));
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [
            $this->username() => [trans('auth.failed')],
        ];
        // Load user from database
        $user = User::where($this->username(), $request->{$this->username()})->first();
        if ($user && $user->role == 'student' && $user->is_logged_in) {
            $errors = [
                $this->username() => ["This user already logged in on another device.if you want to login to this device please contact your teacher."],
            ];
        }
        throw ValidationException::withMessages($errors);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $login = request()->input('login');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        request()->merge([$fieldType => $login]);

        return $fieldType;
    }

}
