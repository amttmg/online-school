<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\PreRegistrationRequest;
use App\PreRegistration;
use App\Providers\RouteServiceProvider;
use App\User;
use http\Env\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $id = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'min:10', 'max:10', 'unique:users'],
            'class' => ['required', 'string', 'max:15'],
            'section' => ['required', 'string', 'max:15'],
            'username' => ['required', 'string', 'max:255', 'unique:users', 'alpha_dash'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:3', 'confirmed'],
        ]);

        return $id;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'class' => $data['class'],
            'section' => $data['section'],
            'username' => $data['username'],
            'email' => $data['email'],
            'role' => 'student',
            'is_verified' => false,
            'is_logged_in' => 1,
            'password' => Hash::make($data['password']),
        ]);
    }

    public function preRegistration()
    {
        return view('auth.pre-registration');
    }

    public function preRegistrationStore(PreRegistrationRequest $request)
    {
        $pre = PreRegistration::create(
            $request->all()
        );

        return back()->with('message',
            ['text' => 'Thank you for pre register for class XI. We will contact you very soon.', 'type' => 'success']);
    }
}
