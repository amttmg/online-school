<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('get-section/{id}', function ($id) {
    return getSections($id);
})->name('get-section');

Route::get('pre-registration', 'Auth\RegisterController@preRegistration')->name('pre.registration');
Route::post('pre-registration', 'Auth\RegisterController@preRegistrationStore')->name('pre.registration.store');

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::namespace('Admin')->prefix('admin')->group(function () {
        Route::get('students', 'AdminController@students')->name('admin.students');
        Route::get('teachers', 'AdminController@teachers')->name('admin.teachers');
        Route::get('approve/{id}', 'AdminController@approve')->name('admin.approve');
        Route::get('reject/{id}', 'AdminController@reject')->name('admin.reject');
        Route::get('reset-device/{id}', 'AdminController@resetDevice')->name('admin.reset');
        Route::get('reset/all', 'AdminController@resetDeviceAll')->name('reset.all');
        Route::get('reset-password/{id}', 'AdminController@resetPassword')->name('admin.reset');
        Route::get('meeting/active-toggle/{id}',
            'OnlineClassController@activeToggle')->name('admin.o-class.active-toggle');
        Route::resource('o-class', 'OnlineClassController');

        Route::get('reports', 'AdminController@reports');
        Route::get('reports/pre-registration',
            'AdminController@preRegistrationReports')->name('reports.pre-registration');
        Route::post('reports/attendance', 'AdminController@attendanceReport')->name('reports.attendance');
    });
    Route::namespace('Student')->prefix('student')->group(function () {
        Route::get('set-attendance/{id}', 'StudentController@setAttendance')->name('student.setAttendance');
    });


    Route::namespace('Api')->prefix('api')->group(function () {
        Route::get('get-classes', 'ApiController@getClasses')->name('api.get.classes');
        Route::get('get-sections/{id}', 'ApiController@getSections')->name('api.get.sections');
    });
});
